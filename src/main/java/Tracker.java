import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;


/**
 * This class navigates the drone with the aim of keeping a face in the middle of the screen and at the same distance as it was when the tracking function was activated.
 * @author Fredrik Foss
 */
public class Tracker extends Thread{

	private final DroneCommander drone;
	private final CameraListener cam;
	private final int DEADZONE_X = 50;
	private final int DEADZONE_Y = 20;
	private final int DEADZONE_Z = 7;

	// x, y is the position of the middle of the face we're tracking (screen coordinates).
	private int prevX, prevY;
	private int initialWidth, initialHeight, curWidth, curHeight; // used for relative distance
	private Rect face = null;
	private boolean alive = true;

	public Tracker(DroneCommander drone, CameraListener cam){
		this.drone = drone;
		this.cam = cam;
	}

	/**
	 * Tells the main loop to stop doing its thing so that this thread can shut down.
	 */
	public void die(){
		alive = false;
	}

	@Override
	public void run() {
		while(alive){
			findSameFace();
			if(face == null) {
				drone.hover();
				while(face == null){
					if(!alive) break; // in case you turn off tracking while no person is in frame.
					System.out.println("NO FACE DETECTED");
					findSameFace();
				}
			}
			if(!alive) break;
			this.prevX = getFaceX();
			this.prevY = getFaceY();
			int offsetX = getFaceX() - CameraListener.CAMERA_WIDTH/2;
			int offsetY = getFaceY() - CameraListener.CAMERA_HEIGHT/2;
			int offsetZ = curWidth - initialWidth + curHeight - initialHeight;  // using both dimensions because people have different face proportions.
			//System.out.println(String.format("target is %d pixels to the %s.", Math.abs(offsetX), offsetX>0 ? "right" : "left"));
			if(Math.abs(offsetX) > DEADZONE_X){
				if(offsetX > 0){ // target is to the right.
					//drone.hoverRight();
					drone.getCommandManager().goRight(Math.round(offsetX/DEADZONE_X));
				} else{
					//drone.hoverLeft();
					drone.getCommandManager().goLeft(Math.round(Math.abs(offsetX)/DEADZONE_X));
				}
			} else if(Math.abs(offsetY) > DEADZONE_Y){
				if(offsetY > 0){ // target has downs.
					//drone.goDown();
					drone.getCommandManager().down(Math.round(offsetY)/DEADZONE_Y);
				} else{
					//drone.goUp();
					drone.getCommandManager().up(Math.round(Math.abs(offsetY)/DEADZONE_Y));
				}
			} else if(Math.abs(offsetZ) > DEADZONE_Z){
				if(offsetZ > 0){ // target is too close.
					//drone.hoverBackward();
					drone.getCommandManager().backward(Math.round(offsetZ)/DEADZONE_Z);
				} else{
					//drone.hoverForward();
					drone.getCommandManager().forward(Math.round(Math.abs(offsetZ)/DEADZONE_Z));
				}
			} else{ // target is within the deadzone
				drone.hover();
			}
		}
	}

	/**
	 * Gets the x coordinate of the middle of the face.
	 */
	private int getFaceX(){
		return face.x + (face.width/2);
	}
	
	/**
	 * Gets the Y coordinate of the middle of the face.
	 */
	private int getFaceY(){
		return face.y + (face.height/2);
	}

	/**
	 * Gets new face coordinates from CameraListener, then locks on to the face that's the closest to the previous face location.
	 * by "locks on to the face" i mean sets the face variable to that rectangle.
	 */
	private void findSameFace(){
		MatOfRect faces = null;
		try {
			faces = cam.getFaceCoords();
		} catch (InterruptedException ex) {
			System.out.println("CameraListener did not have face coordinates stored.");
		}
		face = null;
		int bestDistance = 9000; // a good score is a low score.
		for (Rect rect : faces.toArray()) {
			// only checking distance sideways for good performance. for more precision, do some pythagoras shit with height. add weight for size of rect.
			int offsetx = Math.abs(rect.x + (rect.width/2) - this.prevX);
            if(offsetx < bestDistance){
				bestDistance = offsetx;
				face = rect;
				curWidth = rect.width;
				curHeight = rect.height;
			}
        }
	}

}
