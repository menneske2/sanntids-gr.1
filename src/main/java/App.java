import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.opencv.core.*;

/**
 * @author Fredrik Foss
 */
public class App extends Application {

    public GUIController guiController;

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader fxmlLoader = new FXMLLoader();
        Pane root = fxmlLoader.load(getClass().getResource("gui.fxml").openStream());
        guiController = (GUIController) fxmlLoader.getController();

        Scene scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

	/**
	 * Overrides what happens when you close the application. Our version shuts off the drone too.
	 */
    @Override
    public void stop() {
        guiController.stop();
        System.exit(0);
    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        launch(args);
    }
}
