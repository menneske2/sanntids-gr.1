import de.yadrone.base.command.CommandManager;
import de.yadrone.base.command.LEDAnimation;


/*
 * This class send specific commands to the drone.
 */

/**
 *
 * @author Fredrik Foss
 */
public class DroneCommander {

	private final CommandManager commandManager;
	private boolean isAirBound;
        private int speed = 5;

    /*
     * Constructor.
     * @param commandManager
     */
    public DroneCommander(CommandManager commandManager){
		this.commandManager = commandManager;
    }
	
	public CommandManager getCommandManager(){
		return commandManager;
	}

    /*
     * If isAirBound is set to true, the landing() command will run and the LEDs
     * will be set to standard.
     * If isAirBound is set to false, the LEDs will blink orange, the it will
     * intitialize the ground it's standing on as flat and do this for five
     * seconds, then the LEDs will alternate between blinking red and green,
     * then take off for two seconds, and then hover.
     */
    public void takeOffAndLand() {
        if(isAirBound) {
            commandManager.landing();
            this.setIsAirBound(false);
            commandManager.setLedsAnimation(LEDAnimation.STANDARD, 1, 0);
		} else {
            commandManager.setLedsAnimation(LEDAnimation.BLINK_ORANGE, 3, 5);
            commandManager.flatTrim().doFor(5000);
            commandManager.setLedsAnimation(LEDAnimation.SNAKE_GREEN_RED, 1, 0);
            commandManager.takeOff().doFor(2000);
            this.setIsAirBound(true);
            commandManager.hover();
        }
    }

	public void rotateRight() {
	    if(isAirBound) {
            commandManager.spinRight(speed);
        }
	}

	public void rotateLeft() {
	    if(isAirBound) {
            commandManager.spinLeft(speed);
        }
	}

	public void hoverForward() {
		if(isAirBound) {
            commandManager.forward(speed);
        }
	}

	public void hoverRight() {
	    if(isAirBound) {
            commandManager.goRight(speed);
        }
	}

	public void hoverBackward() {
	    if(isAirBound) {
            commandManager.backward(speed);
        }
	}

	public void hoverLeft() {
	    if(isAirBound) {
            commandManager.goLeft(speed);
        }
	}
	
	public void goUp(){
		if(isAirBound)
			commandManager.up(Math.round(speed/2));
	}
	
	public void goDown(){
		if(isAirBound)
			commandManager.down(Math.round(speed/2));
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * Tries to not move.
	 */
	public void hover() {
	    if(isAirBound) {
            commandManager.hover();
        }
	}
  
    public void setIsAirBound(boolean isAirBound) {
        this.isAirBound = isAirBound;
    }
}
