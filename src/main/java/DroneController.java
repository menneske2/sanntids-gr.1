import de.yadrone.base.ARDrone;
import de.yadrone.base.exception.ARDroneException;
import de.yadrone.base.exception.IExceptionListener;
import de.yadrone.base.navdata.Altitude;
import de.yadrone.base.navdata.AltitudeListener;
import de.yadrone.base.navdata.AttitudeListener;
import de.yadrone.base.navdata.BatteryListener;
import javafx.scene.paint.Color;

/*
 * This class controls the setup of the drone.
 */
public class DroneController {

	private final int FREE_ROAMING = 0;
	private final int TRACKING = 1;

    private final GUIController gui;
    private ARDrone drone;
    private CameraListener cam;
	private Tracker tracker = null;
    private int previousBattery = 100;
	private int mode = FREE_ROAMING;

    private DroneCommander droneCommander;

    boolean connected = false;

	
    public DroneController(GUIController gui) {
        this.gui = gui;
    }


    public ARDrone getDrone() {
        return drone;
    }

	/**
	 * Initializes or stops the tracking operation.
	 */
	public void track(){
		if(mode == TRACKING){  // quit tracking
			mode = FREE_ROAMING;
			tracker.die();
			try {
				tracker.join();
			} catch (InterruptedException ex) {
				System.out.println("cant stop wont stop");
			}
		}
		else{  // start tracking
			mode = TRACKING;
			tracker = new Tracker(droneCommander, cam);
			tracker.start();
		}
	}

    /*
     * Connects the computer network to the drone wifi.
     * @return whether or not it was successful.
     */
    public boolean connect() {
        if(drone == null) {
            try
            {
                drone = new ARDrone();
                drone.start();
                setup();
            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                if (drone != null)
                    drone.stop();
                System.exit(0);
            }

            connected = true;
            drone.getCommandManager().setMaxAltitude(1500);
            drone.getCommandManager().setOutdoor(false, true);
            cam = new CameraListener(gui, this);
            droneCommander = new DroneCommander(drone.getCommandManager());
            return true;
        } else {
            drone.stop();
            return true;
        }
    }

    /*
     * Initialize event listeners for when the drone sends data. Camera listening is taken care of in the CameraListener class.
     */
    private void setup() {
        drone.addExceptionListener(new IExceptionListener() {
            public void exeptionOccurred(ARDroneException exc) {
                exc.printStackTrace();
            }
        });
        drone.getNavDataManager().addAttitudeListener(new AttitudeListener() {

            public void attitudeUpdated(float pitch, float roll, float yaw) {
                //	System.out.println("Pitch: " + pitch + " Roll: " + roll + " Yaw: " + yaw);
            }

            public void attitudeUpdated(float pitch, float roll) {
            }

            public void windCompensation(float pitch, float roll) {
            }
        });

        drone.getNavDataManager().addBatteryListener(new BatteryListener() {

            public void batteryLevelChanged(int percentage) {
                if (percentage < previousBattery) {
                    previousBattery = percentage;
                    //System.out.println("Battery: " + percentage + " %");
                    if(percentage < 25) {
                        gui.getBattery().setTextFill(Color.RED);
                    } else if (percentage < 50) {
                        gui.getBattery().setTextFill(Color.ORANGE);
                    } else {
                        gui.getBattery().setTextFill(Color.GREEN);
                    }
                    gui.setBatteryText(percentage);
                }
            }

            public void voltageChanged(int vbat_raw) {
            }
        });

        drone.getNavDataManager().addAltitudeListener(new AltitudeListener() {
            @Override
            public void receivedAltitude(int i) {
//				System.out.println("altitude: " + i);
            }

            @Override
            public void receivedExtendedAltitude(Altitude altd) {

            }
        });
    }

    /*
     * Shuts down the drone
     */
    public void stop() {
        if (connected) {
            drone.stop();
            connected = false;
        }
    }

 
    public DroneCommander getDroneCommander() {
        return droneCommander;
    }
}
