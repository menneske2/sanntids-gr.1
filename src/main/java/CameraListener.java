import de.yadrone.base.video.ImageListener;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import org.opencv.objdetect.CascadeClassifier;

/*
 * This class fetches and processes the camera feed.
 */

/**
 * @author Fredrik Foss
 */
public class CameraListener {

	public static int CAMERA_WIDTH = 0;
	public static int CAMERA_HEIGHT = 0;

    private final DroneController droneController;
    private final GUIController gui;

    private BufferedImage image = null;
	private MatOfRect faceCoordinates = null;

	CascadeClassifier faceDetector = new CascadeClassifier(
				System.getProperty("user.dir") + "\\src\\main\\resources\\face.xml");

    int processEveryNthImg = 8;
    int currentImgCount = processEveryNthImg;


    public CameraListener(GUIController gui, DroneController droneController) {
        this.gui = gui;
        this.droneController = droneController;
        establishCameraFeed();
    }

    /*
     * Finds the coordinates of all faces on screen.
     */
    public synchronized MatOfRect getFaceCoords() throws InterruptedException{
        while (faceCoordinates == null)
            wait();
        MatOfRect temp = faceCoordinates;
        faceCoordinates = null;
        return temp;
    }

    /*
     * Converts a Mat to a BufferedImage
     */
    public static BufferedImage Mat2BufferedImage(Mat matrix) {
        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(".jpg", matrix, mob);
        byte ba[] = mob.toArray();

        BufferedImage bi = null;
        try {
            bi = ImageIO.read(new ByteArrayInputStream(ba));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bi;
    }

    /*
     * Converts a BufferedImage to a Mat
     */
    public static Mat bufferedImageToMat(BufferedImage bi) {
        Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }

    /*
     * Establishes a camera feed between the app and the drone.
     */
    private void establishCameraFeed() {
        droneController.getDrone().getVideoManager().addImageListener(new ImageListener() {
            @Override
            public void imageUpdated(BufferedImage newImage) {
                currentImgCount++;
                if (currentImgCount >= processEveryNthImg) {
                    currentImgCount = 0;
                    image = newImage;
                    processImage();
                }
            }
        });
    }

    /*
     * Performs face recognition on the picture, stores the face coordinates for the tracker and sends the image to the GUI.
     */
    private void processImage() {
        if (image == null) {
            System.out.println("no img :(");
            return;
        }

		final Image main = SwingFXUtils.toFXImage(Mat2BufferedImage(detectFaces(bufferedImageToMat(image))), null);

        // push the images to the GIU.
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (gui != null) {
                    gui.setViews(main);
                }
            }
        });
    }

    /*
     * Detects all faces in the image received in the camera feed, puts a green rectangle around them and returns the image.
     * @param img Image to scan for faces.
     * @return img Image with green squares around all found faces.
     */
	private synchronized Mat detectFaces(Mat img){

		CAMERA_WIDTH = img.cols();
		CAMERA_HEIGHT = img.rows();
		faceCoordinates = new MatOfRect();
        faceDetector.detectMultiScale(img, faceCoordinates);
        notifyAll();

		for (Rect rect : faceCoordinates.toArray()) {
				Imgproc.rectangle(img, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
				new Scalar(0, 255, 0));
		}
		return img;
    }
}

