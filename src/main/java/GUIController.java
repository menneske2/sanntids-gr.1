import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import java.awt.event.*;

/*
 * This class controls the gui
 */
public class GUIController {

    private DroneController missDaisy;

    private final int ERROR = 0;
    private final int WARNING = 1;
    private final int INFO = 2;

    @FXML
    private ImageView imageFeed;

    @FXML
    private Button connect;
    @FXML
    private Slider speedSlider;
    @FXML
    private Button rleft;
    @FXML
    private Button rright;
    @FXML
    private Button forward;
    @FXML
    private Button takeoff;
    @FXML
    private Button left;
    @FXML
    private Button right;
    @FXML
    private Button backward;

    @FXML
    private TitledPane battery;
    @FXML
    private TitledPane connectStatus;

    @FXML
    private VBox terminalScroll;

    @FXML
    private void connectToDrone(ActionEvent event) {
        addTextToTerminal(INFO, "Connecting to drone..");
        if (missDaisy.connect()) {
            addTextToTerminal(INFO, "Connected to drone");
            connectStatus.setTextFill(Color.GREEN);
            connectStatus.setText("Connection - Connected");
        } else {
            addTextToTerminal(ERROR, "Failed to connect to drone, check that wifi is connected to drone");
            connectStatus.setTextFill(Color.RED);
            connectStatus.setText("Connection - Failed");
        }
    }

	/**
	 * Takes off if the drone is grounded. Otherwise, lands it.
	 */
    @FXML
    private void takeOffLand() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().takeOffAndLand();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

	/**
	 * Starts/stops tracking of person.
	 */
    @FXML
    public void track(ActionEvent e) {
        if (missDaisy.connected) {
            missDaisy.track();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void rotateRight() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().rotateRight();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void rotateRightReleased() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hover();
        }
    }
    
	/**
	 * Keyboard controls
	 */
    private void KeyPressed(KeyEvent e) {
        if(missDaisy.connected) {
            int id = e.getID();
            if(id == KeyEvent.KEY_PRESSED) {
                int keyCode = e.getKeyCode();
                String s = KeyEvent.getKeyText(keyCode);
                if(s.equals("e") || s.equals("E")) {
                    missDaisy.getDroneCommander().rotateRight();
                }
                if(s.equals("q") || s.equals("Q")) {
                    missDaisy.getDroneCommander().rotateLeft();
                }
                if(s.equals("a") || s.equals("A")) {
                    missDaisy.getDroneCommander().hoverLeft();
                }
                if(s.equals("s") || s.equals("S")) {
                    missDaisy.getDroneCommander().hoverBackward();
                }
                if(s.equals("d") || s.equals("D")) {
                    missDaisy.getDroneCommander().hoverRight();
                }
                if(s.equals("w") || s.equals("W")) {
                    missDaisy.getDroneCommander().hoverForward();
                }
            }
            if(id == KeyEvent.KEY_RELEASED) {
                missDaisy.getDroneCommander().hover();
            }
        }
    }


    @FXML
    private void rotateLeft() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().rotateLeft();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void rotateLeftReleased() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hover();
        }
    }

    @FXML
    private void hoverForward() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hoverForward();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void hoverForwardReleased() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hover();
        }
    }

    @FXML
    private void hoverRight() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hoverRight();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void hoverRightReleased() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hover();
        }
    }

    @FXML
    private void hoverBackwards() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hoverBackward();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void hoverBackwardsReleased() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hover();
        }
    }

    @FXML
    private void hoverLeft() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hoverLeft();
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void hoverLeftReleased() {
        if (missDaisy.connected) {
            missDaisy.getDroneCommander().hover();
        }
    }

    @FXML
    private void updateSpeed() {
        if (missDaisy.connected) {
            //TODO: something
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void updateAltitude() {
        if (missDaisy.connected) {
            //TODO: something
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    @FXML
    private void updateFPS() {
        if (missDaisy.connected) {
            //TODO: something
        } else {
            addTextToTerminal(WARNING, "Drone is not connected");
        }
    }

    private void addTextToTerminal(int type, String content) {
        Text text = new Text(content);
        switch (type) {
            case ERROR:
                text = new Text("ERROR - " + content);
                text.setFill(Color.RED);
                break;

            case WARNING:
                text = new Text("WARNING - " + content);
                text.setFill(Color.ORANGE);
                break;

            case INFO:
                text = new Text("INFO - " + content);
                text.setFill(Color.BLUE);
                break;
        }
        terminalScroll.getChildren().add(text);
    }

    public TitledPane getBattery() {
        return battery;
    }

    public void initialize() {
        missDaisy = new DroneController(this);
        speedSlider.valueProperty().addListener(
                (observable, oldvalue, newvalue) ->
                {
                    int i = newvalue.intValue();
                    if(missDaisy.getDroneCommander() != null) {
                        missDaisy.getDroneCommander().setSpeed(i);
                    }
                });
    }

	/**
	 * Updates the image on the camera.
	 * @param main a JavaFX image.
	 */
    public void setViews(Image main) {
        imageFeed.setImage(main);
    }

	/**
	 * shuts down the drone.
	 */
    public void stop() {
        missDaisy.stop();
    }

    public void setBatteryText(int percentage) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                getBattery().setText("Battery - " + percentage + " %");
            }
        });
    }
}
